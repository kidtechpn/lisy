import React from 'react';
import GlobalStyles from './GlobalStyles';
import Home from './pages/home/index';
import About from './pages/about';
import Projects from './pages/projects';


function App() {
  return (
 <>
    <GlobalStyles/>
    <Home/>
    <About/>
    <Projects/>
 </>


  );
}

export default App;
