import React from 'react';

import NavBar from '../../components/NavBar';
import Section from '../../components/Section';
import Info from '../../components/Info';
import InfoImg from '../../components/InfoImg';
import SocialMedia from '../../components/SocialMedia';
import Buttom from '../../components/Buttom';
import facebook from '../../assets/facebook.svg'
import github from '../../assets/imagem-do-github.svg'
import linkedin from '../../assets/linkedin.svg'
import gmail from '../../assets/gmail.svg'

const info ={
  hello:"Hello I'm",
  title:'Lisiane Lima',
  description:'Lorem impsum dolor iset Lorem impsum dolor iset Lorem impsum dolor iset Lorem impsum dolor iset Lorem impsum dolor iset Lorem impsum dolor isetLorem impsum dolor iset Lorem impsum dolor iset Lorem impsum dolor iset Lorem impsum dolor iset Lorem impsum dolor iset Lorem impsum dolor iset'
}
const social=[
  {link:"facebook.com/lisianelima",icon:facebook, name:"facebook"},
  {link:"facebook.com/lisianelima",icon:github, name:"github"},
  {link:"facebook.com/lisianelima",icon:linkedin, name:"linkedin"},
  {link:"facebook.com/lisianelima",icon:gmail, name:"gmail"},
]

function Home() {
  return (
  <>
   <NavBar ></NavBar>
   <Section height="600" background="#e5e5e5" id="home">
     <Info hello={info.hello} title={info.title} description={info.description}>
     <div>
      <Buttom primary name="My Works"/>
      <Buttom name="About me"/>
      </div>
      <SocialMedia social={social}></SocialMedia>
     </Info>
     <InfoImg></InfoImg>
   </Section>



  </>
  );
}

export default Home;