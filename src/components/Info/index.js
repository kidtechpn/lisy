import React from 'react';
import { PropTypes } from 'prop-types'

 import { Container } from './styles';


function Info({children, hello,title,description, background}) {

  return(
    <Container background={background}>
      <h4>{hello}</h4>
      <h1>{title}</h1>
      <p>
      {description}
      </p>
      {children}
    </Container>
  );
}

Info.propTypes = {
  hello:PropTypes.string,
  title:PropTypes.string,
  description:PropTypes.string,
  background:PropTypes.string,
}
export default Info;