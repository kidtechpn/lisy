import React from 'react';

 import { Container } from './styles';
 import img from '../../assets/img.jpg'

function InfoImg() {
  return (
    <Container>
      <img src={img} alt="foto"/>
    </Container>
  );
}

export default InfoImg;