import React from 'react';

import { Container } from './styles';
import NavItem from '../NavBarItem';

import Logo from '../Logo';

function NavBar() {

  const links = [
    {icon:"home", link:"#home", name:'home'},
    {icon:"info", link:"#about", name:'about'},
    {icon:"folder", link:"#project", name:'project'},
    {icon:"phone", link:"#contact", name:'contact'},
  ]

  return (
    <Container>
    <Logo></Logo>
      <NavItem items={links} />
    </Container>
  );
}

export default NavBar;