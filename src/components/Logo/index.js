import React from 'react';

import { Container } from './styles';

function Logo() {
  return <Container>
    Lisiane Lima Portfolio
  </Container>;
}

export default Logo;