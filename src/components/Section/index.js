import React from 'react';

import { Container } from './styles';

function Section({children, id, height, background}) {
  return (
    <Container id={id} height={height} background={background}>
      {children}
    </Container>
  );
}

export default Section;